export const bannerTextContent = {
  ["treinamentos-certificacao-home"]: {
    title: `Treinamento e certificação`,
    text: [
      {
        className: '',
        content: `O portfólio de cursos mais completo da América Latina com mais de`
      },
      {
        className: 'text-color-primary destaque-500',
        content: `300 TREINAMENTOS E 40 CERTIFICAÇÕES`
      }
    ]
  },
  ["graduacao-home"]: {
    title: `Graduação`,
    text: [
      {
        className: '',
        content: `Sed vel risus viverra libero tincidunt imperdiet vel et nunc. Aliquam id dignissim viverra libero tincidunt imperdiet`
      }
    ]
  },
  ["pos-mba-home"]: {
    title: `Pós e MBA`,
    text: [
      {
        className: '',
        content: `Sed vel risus viverra libero tincidunt imperdiet vel et nunc. Aliquam id dignissim viverra libero tincidunt imperdiet`
      }
    ]
  },
  ["escola-tecnica-home"]: {
    title: `Escola Técnica`,
    text: [
      {
        className: '',
        content: `Sed vel risus viverra libero tincidunt imperdiet vel et nunc. Aliquam id dignissim viverra libero tincidunt imperdiet`
      }
    ]
  },
  ["corporativo-home"]: {
    title: `Corporativo`,
    text: [
      {
        className: '',
        content: `Sed vel risus viverra libero tincidunt imperdiet vel et nunc. Aliquam id dignissim viverra libero tincidunt imperdiet`
      }
    ]
  }
}