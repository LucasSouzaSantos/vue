## Bem-vindo à Impacta!
Agradecemos por utilizar nossos produtos e serviços. Todos os serviços são fornecidos pela Impacta Treinamentos, localizado na Av. Paulista, 1.009, Cerqueira César, São Paulo, SP, CEP 01311-100, Brasil.

Ao usar nossos Serviços, você está concordando com estes termos. Leia-os com atenção.

## 1) Da Aceitação
1.1) O uso deste site implica, impreterivelmente, a aceitação das condições de uso abaixo descritas.

## 2) Do Conteúdo
2.1) Salvo observação em contrário, todo o conteúdo exposto neste site é de propriedade da IMPACTA, de terceiros que cederem o seu direito de uso ou ainda, de domínio público.

2.2) Somente mediante autorização expressa da IMPACTA, será permitido ao usuário do site reproduzir, transferir, distribuir ou armazenar, de forma parcial ou na íntegra, o conteúdo disponibilizado no site. Não se incluem na presente vedação a impressão de material escrito e gráfico para uso exclusivamente pessoal.

2.3) Os documentos de domínio público disponibilizados neste site somente poderão ser utilizados para uso privado e não comercial.

## 3) Da Responsabilidade
3.1) Todo o conteúdo disponibilizado no site é oferecido no “estado em que se encontra”, podendo ser alterado a qualquer momento e não sendo garantida a sua permanente disponibilidade.

3.2) A IMPACTA não garante que o conteúdo do site, bem como os serviços eventualmente neste oferecidos, estejam precisamente atualizados ou completos, de modo que não se responsabiliza por eventuais danos causados por erros de conteúdo ou falhas no equipamento do usuário.

3.4) A IMPACTA não se responsabiliza pelo uso indevido do conteúdo disponibilizado no site, como também não responde pelos danos que porventura poderão resultar deste ato.

3.5) Não é oferecida nenhuma garantia de que a disponibilização do site não sofra interrupção por problemas de ordem técnica.

3.6) O site pode conter links para sites operados por terceiros, sendo que, nesta hipótese, a IMPACTA não será responsável pelo seu conteúdo, tampouco pela contínua disponibilidade de acesso. Neste ato, o usuário declara-se ciente de que a IMPACTA não tem qualquer controle sobre o conteúdo de tais sites e não pode assumir qualquer responsabilidade sobre materiais criados ou publicados por estes.

3.7) O provimento adequado de todos os recursos de software e hardware necessário para o acesso adequado ao site é de exclusiva responsabilidade do usuário.

## 4) Da Disponibilidade Técnica e da Manutenção do Site
4.1) A IMPACTA, nos termos da legislação em vigor, garante a veracidade das informações prestadas no site, em especial no tocante as descrições técnicas e de recursos dos produtos oferecidos. Todavia, a IMPACTA se reserva os seguintes direitos:

a) As características dos produtos expostos neste site podem ser alteradas a qualquer momento, sem aviso prévio;

b) Os produtos e serviços expostos à comercialização poderão ser alterados ou excluídos temporária ou permanentemente do site;

c) Os preços dos produtos indicados no site poderão ser alterados a qualquer momento sem aviso prévio, salvo quando especificado uma data de validade para os mesmos;

4.2) Em razão de manutenção, atualização ou mesmo por problemas técnicos, o site poderá a qualquer momento ficar desativado temporariamente.

4.3) A IMPACTA se reserva o direito de, a seu livre critério, alterar as funcionalidades deste site, visando a melhoria do seu acesso, bem como da visualização e utilização do conteúdo.

## 5) Dos Serviços Oferecidos no Site
5.1) Junto ao conteúdo disponibilizado no site poderão ser ofertados alguns serviços ao usuário, dentre os quais:

a) A abertura de chamado técnico, casos em que serão observados os Termos de Garantia da empresa IMPACTA;

b) A venda online (e-commerce) de produtos da IMPACTA, conforme Termos e Condições de Venda desta empresa;

c) Disponibilização de downloads de drivers para a adequada instalação e utilização dos equipamentos e serviços disponibilizados pela IMPACTA.
